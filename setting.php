<?php
include_once("idiorm/idiorm.php");

ORM::configure(array(
    'connection_string' => 'mysql:host=localhost:8889;dbname=shopclip_db',
    'username' => 'root',
    'password' => 'root'
));
ORM::configure('return_result_sets', true);