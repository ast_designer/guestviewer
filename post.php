<?php
ini_set( 'display_errors', 1 );
error_reporting(E_ALL & ~ E_DEPRECATED & ~ E_USER_DEPRECATED & ~ E_NOTICE);

include_once("setting.php");
$result = [];

if( isset($_POST["file_name"]) && isset($_POST["base_64_encoded_image"])){

	$img = $_POST['base_64_encoded_image'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$fileData = base64_decode($img);
	$fileName = $_POST["file_name"];
	file_put_contents("image_l/".$fileName, $fileData);

	$src_file = "image_l/".$fileName;
	$dst_file = "image_s/".$fileName;

	list($src_w, $src_h) = getimagesize($src_file);
	$dst_img = imagecreatetruecolor($src_w/4, $src_h/4);

	$src_img = imagecreatefromjpeg($src_file);
	imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $src_w/4, $src_h/4, $src_w, $src_h);	
	imagejpeg($dst_img, $dst_file, 80);

	$shop_clip = ORM::for_table('tbl_photo')->create();
	$shop_clip->filename = $fileName;
	$shop_clip->shop_id = 2;
	$shop_clip->save();

	$result["file_name"]=$_POST["file_name"];
	$result["result"]="OK";
} else {
	$result["result"]="ERR";
}

echo(json_encode($result));
//echo("\n");	
//echo(json_encode($_POST));	
//echo("\n");	
//echo(json_encode($_GET));	
//echo("\n");	
//print_r($_POST);
//foreach($_POST as $key => $value){
//	echo($key."\n");
//}