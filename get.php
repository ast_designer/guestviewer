<?php
ini_set( 'display_errors', 1 );
error_reporting(E_ALL & ~ E_DEPRECATED & ~ E_USER_DEPRECATED & ~ E_NOTICE);

include_once("setting.php");

$query = false;
$result = [];
$result["contents"] = [];

if( isset($_GET["query"]) && strtotime($_GET["query"])){

	$query = true;

}

if( $query ){
	$date = ORM::for_table("tbl_photo")
		->raw_query("SELECT DATE_FORMAT(postdate, '%Y-%m-%d %H:00:00') AS ttime FROM tbl_photo WHERE postdate < '".$_GET["query"]."' GROUP BY ttime ORDER BY ttime DESC limit 3")
		->find_array();
} else {
	$date = ORM::for_table("tbl_photo")
		->raw_query("SELECT DATE_FORMAT(postdate, '%Y-%m-%d %H:00:00') AS ttime FROM tbl_photo GROUP BY ttime ORDER BY ttime DESC limit 3")
		->find_array();
}

foreach( $date as $value){
	$contents = [];
	$photos = ORM::for_table("tbl_photo")
		->where_raw("postdate between ? AND ?",array($value["ttime"],date("Y-m-d H:i:s",strtotime("+1 hour",strtotime($value["ttime"])))))
		->order_by_desc("postdate");
	$contents += array("date"=>$value["ttime"],"photos"=>$photos->find_array());
	array_push($result["contents"],$contents);
}

echo(json_encode($result));